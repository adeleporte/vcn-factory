# configure some variables first
variable "nsx" {
    type = "map"
    description = "NSX Login Details"
}
variable "vrni-provider" {
    type = "map"
    description = "VRNI Login Details"
}
variable "appdefense-provider" {
    type = "map"
    description = "AppDefense Login Details"
}
variable "velocloud-provider" {
    type = "map"
    description = "Velocloud Login Details"
}
variable "vsphere" {
    type = "map"
    description = "vSphere Details"
}
variable "nsx_data_vars" {
    type = "map"
    description = "Existing NSX vars for data sources"
}
variable "nsx_rs_vars" {
    type = "map"
    description = "NSX vars for the resources"
}
variable "nsx_tag_scope" {
    type = "string"
    description = "Scope for the tag that will be applied to all resources"
}
variable "nsx_tag" {
    type = "string"
    description = "Tag, the value for the scope above"
}
variable "ipset" {
    type = "list"
    description = "List of ip addresses that will be add in the IP-SET to allow communication to all VMs"
}

variable "vrni_appname" {
   type= "string"
   description = "VCN App Name"
   default = "MyVCNApp"
}

variable "app_listen_port" {
    type = "string"
    description = "TCP Port the App server listens on"
}

variable "web" {
    type = "map"
    description = "NSX vars for the resources"
}
variable "app" {
    type = "map"
    description = "NSX vars for the resources"
}
variable "db" {
    type = "map"
    description = "NSX vars for the resources"
}

variable "dns_server_list" {
    type = "list"
    description = "DNS Servers"
}

variable "bandwidth" {
  type = "string"
  description = "sdwan bandwidth"
}

variable "quarantine" {
  type = "string"
  default = 0
}

variable "appdefense" {
  type = "string"
  default = "true"
}

variable "wan" {
  type = "string"
  default = "true"
}

variable "vrni" {
  type = "string"
  default = "true"
}

variable "security" {
  type = "string"
  default = "true"
}

variable "vm" {
  type = "string"
  default = "true"
}

variable "networks" {
  type = "string"
  default = "true"
}

variable "threshold" {
  type = "string"
  default = "true"
}

variable "class" {
  type = "string"
  default = "transactional"
}

variable "servicegroup" {
  type = "string"
  default = "ALL"
}
