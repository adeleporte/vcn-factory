provider "appdefense" {
  host           = "${var.appdefense-provider["host"]}"
  username       = "${var.appdefense-provider["username"]}"
  password       = "${var.appdefense-provider["password"]}"
}

data "appdefense_endpoint" "vm1"{
  count = "${var.appdefense == "true" ? 1 : 0}"
  name      = "${vsphere_virtual_machine.webvm.name}"
}

data "appdefense_endpoint" "vm2"{
  count = "${var.appdefense == "true" ? 1 : 0}"
  name      = "${vsphere_virtual_machine.appvm.name}"
}

resource "appdefense_service" "web-service" {
  count = "${var.appdefense == "true" ? 1 : 0}"
  service_id  = 607

  member {
    id  = "${data.appdefense_endpoint.vm1.id}"
    name = "${data.appdefense_endpoint.vm1.name}"
  }
}

resource "appdefense_service" "app-service" {
  count = "${var.appdefense == "true" ? 1 : 0}"
  service_id  = 608

  member {
    id  = "${data.appdefense_endpoint.vm2.id}"
    name = "${data.appdefense_endpoint.vm2.name}"
  }
}
