provider "velocloud" {
  host     = "${var.velocloud-provider["host"]}"
  username = "${var.velocloud-provider["username"]}"
  password = "${var.velocloud-provider["password"]}"
  operator = "${var.velocloud-provider["operator"]}"
}

data "velocloud_configuration" "tf_config" {
  name         = "us_profile"
  enterpriseid = 123
}

resource "velocloud_qos_rule" "tf_qos_rule" {
  count = "${var.wan == "true" ? 1 : 0}"
  enterpriseid    = 123
  configurationid = "${data.velocloud_configuration.tf_config.id}"
  qosmodule       = "${data.velocloud_configuration.tf_config.qosmodule}"

  rule {
    name            = "My VCN App"
    dip             = "${var.web["ip"]}"
    dmask           = "255.255.255.255"
    dport           = 80
    proto           = 6
    bandwidthpct    = "${var.bandwidth}"
    class           = "${var.class}"
    servicegroup    = "${var.servicegroup}"
  }
  
}
