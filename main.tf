provider "nsxt" {
    host = "${var.nsx["ip"]}"
    username = "${var.nsx["user"]}"
    password = "${var.nsx["password"]}"
    allow_unverified_ssl = true
}

# Create the data sources we will need to refer to later
data "nsxt_transport_zone" "overlay_tz" {
    display_name = "${var.nsx_data_vars["transport_zone"]}"
}
data "nsxt_logical_tier0_router" "tier0_router" {
  display_name = "${var.nsx_data_vars["t0_router_name"]}"
}
data "nsxt_edge_cluster" "edge_cluster1" {
    display_name = "${var.nsx_data_vars["edge_cluster"]}"
}

# Create Web Tier NSX-T Logical Switch
resource "nsxt_logical_switch" "web" {
    count = "${var.networks == "true" ? 1 : 0}"
    admin_state = "UP"
    description = "LS created by Terraform"
    display_name = "MyApp-L2-LS"
    transport_zone_id = "${data.nsxt_transport_zone.overlay_tz.id}"
    replication_mode = "MTEP"
    tag {
	scope = "${var.nsx_tag_scope}"
	tag = "${var.nsx_tag}"
    }
}


# Create T1 router
resource "nsxt_logical_tier1_router" "tier1_router" {
  count = "${var.networks == "true" ? 1 : 0}"
  description                 = "Tier1 router provisioned by Terraform"
  display_name                = "${var.nsx_rs_vars["t1_router_name"]}"
  failover_mode               = "PREEMPTIVE"
  edge_cluster_id             = "${data.nsxt_edge_cluster.edge_cluster1.id}"
  enable_router_advertisement = true
  advertise_connected_routes  = true
  advertise_static_routes     = true
  advertise_nat_routes        = true
    tag {
	scope = "${var.nsx_tag_scope}"
	tag = "${var.nsx_tag}"
    }
}

# Create a port on the T0 router. We will connect the T1 router to this port
resource "nsxt_logical_router_link_port_on_tier0" "link_port_tier0" {
  count = "${var.networks == "true" ? 1 : 0}"
  description       = "TIER0_PORT1 provisioned by Terraform"
  display_name      = "TIER0_PORT1"
  logical_router_id = "${data.nsxt_logical_tier0_router.tier0_router.id}"
    tag {
	scope = "${var.nsx_tag_scope}"
	tag = "${var.nsx_tag}"
    }
}

# Create a T1 uplink port and connect it to T0 router
resource "nsxt_logical_router_link_port_on_tier1" "link_port_tier1" {
  count = "${var.networks == "true" ? 1 : 0}"
  description                   = "TIER1_PORT1 provisioned by Terraform"
  display_name                  = "TIER1_PORT1"
  logical_router_id             = "${nsxt_logical_tier1_router.tier1_router.id}"
  linked_logical_router_port_id = "${nsxt_logical_router_link_port_on_tier0.link_port_tier0.id}"
    tag {
	scope = "${var.nsx_tag_scope}"
	tag = "${var.nsx_tag}"
    }
}


# Create a switchport on Web logical switch
resource "nsxt_logical_port" "logical_port2" {
  count = "${var.networks == "true" ? 1 : 0}"
  admin_state       = "UP"
  description       = "LP1 provisioned by Terraform"
  display_name      = "MyVCNAppLSToT1"
  logical_switch_id = "${nsxt_logical_switch.web.id}"
    tag {
	scope = "${var.nsx_tag_scope}"
	tag = "${var.nsx_tag}"
    }
}

# Create downlink port on the T1 router and connect it to the switchport we created earlier
resource "nsxt_logical_router_downlink_port" "downlink_port2" {
  count = "${var.networks == "true" ? 1 : 0}"
  description                   = "DP2 provisioned by Terraform"
  display_name                  = "DP2"
  logical_router_id             = "${nsxt_logical_tier1_router.tier1_router.id}"
  linked_logical_switch_port_id = "${nsxt_logical_port.logical_port2.id}"
  ip_address                    = "${var.web["gw"]}/${var.web["mask"]}"
    tag {
	scope = "${var.nsx_tag_scope}"
	tag = "${var.nsx_tag}"
    }
}


# Configure the VMware vSphere Provider
provider "vsphere" {
    user           = "${var.vsphere["vsphere_user"]}"
    password       = "${var.vsphere["vsphere_password"]}"
    vsphere_server = "${var.vsphere["vsphere_ip"]}"
    allow_unverified_ssl = true
}

# data source for my vSphere Data Center
data "vsphere_datacenter" "dc" {
  name = "${var.vsphere["dc"]}"
}


resource "null_resource" "delay" {
  count = "${var.networks == "true" ? 1 : 0}"
  provisioner "local-exec" {
    command = "sleep 10"
  }
  triggers = {
    "before" = "${nsxt_logical_switch.web.id}"
  }
}

resource "null_resource" "delaydestroy" {
  count = "${var.networks == "true" ? 1 : 0}"
  depends_on = ["nsxt_logical_switch.web"]
  provisioner "local-exec" {
    command = "sleep 120"
    when = "destroy"
  }
}

# Data source for the logical switch we created earlier
# we need that as we cannot refer directly to the logical switch from the vm resource below
data "vsphere_network" "terraform_web" {
    count = "${var.networks == "true" ? 1 : 0}"
    name = "${nsxt_logical_switch.web.display_name}"
    datacenter_id = "${data.vsphere_datacenter.dc.id}"
    depends_on = ["nsxt_logical_switch.web", "null_resource.delay"]
}

# Datastore data source
data "vsphere_datastore" "datastore" {
  name          = "${var.vsphere["datastore"]}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

# data source for my cluster's default resource pool
data "vsphere_resource_pool" "pool" {
  name          = "${var.vsphere["resource_pool"]}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

# Data source for the template I am going to use to clone my VM from
data "vsphere_virtual_machine" "template" {
    name = "${var.vsphere["vm_template"]}"
    datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

# Clone a VM from the template above and attach it to the newly created logical switch
resource "vsphere_virtual_machine" "appvm" {
    count = "${var.vm == "true" ? 1 : 0}"
    name             = "${var.app["vm_name"]}"
    depends_on = ["nsxt_logical_switch.web", "null_resource.delaydestroy"]
    resource_pool_id = "${data.vsphere_resource_pool.pool.id}"
    datastore_id     = "${data.vsphere_datastore.datastore.id}"
    num_cpus = 1
    memory   = 4096
    guest_id = "${data.vsphere_virtual_machine.template.guest_id}"
    scsi_type = "${data.vsphere_virtual_machine.template.scsi_type}"
    # Attach the VM to the network data source that refers to the newly created logical switch
    network_interface {
      network_id = "${data.vsphere_network.terraform_web.id}"
    }
    disk {
	label = "${var.app["vm_name"]}.vmdk"
        size = 16
        thin_provisioned = true
    }
    clone {
	template_uuid = "${data.vsphere_virtual_machine.template.id}"

	# Guest customization to supply hostname and ip addresses to the guest
	customize {
	    linux_options {
		host_name = "${var.app["vm_name"]}"
		domain = "${var.app["domain"]}"
	    }
	    network_interface {
		ipv4_address = "${var.app["ip"]}"
		ipv4_netmask = "${var.app["mask"]}"
		dns_server_list = "${var.dns_server_list}"
		dns_domain = "${var.app["domain"]}"
	    }
	    ipv4_gateway = "${var.app["gw"]}"
	}
    }
    connection {
	type = "ssh",
	agent = "false"
	host = "${var.app["nat_ip"] != "" ? var.app["nat_ip"] : var.app["ip"]}"
	user = "${var.app["user"]}"
	password = "${var.app["pass"]}"
	script_path = "/root/tf.sh"
    }
    provisioner "remote-exec" {
	inline = [
	    "echo 'nameserver ${var.dns_server_list[0]}' >> /etc/resolv.conf",
            "echo 'search ${var.app["domain"]}' >>/etc/resolv.conf"
	]
    }
}

resource "nsxt_vm_tags" "vm1_tags" {
    count = "${var.security == "true" ? 1 : 0}"
    instance_id = "${vsphere_virtual_machine.appvm.id}"
    tag {
        scope = "${var.nsx_tag_scope}"
        tag = "${var.nsx_tag}"
    }
    tag {
        scope = "tier"
        tag = "app"
    }
    tag {
        scope = "os"
        tag = "ubuntu1604"
    }
    tag {
        scope = "appdefense/scope"
        tag = "cpod-vcn"
    }
}

# Clone a VM from the template above and attach it to the newly created logical switch
resource "vsphere_virtual_machine" "webvm" {
    count = "${var.vm == "true" ? 1 : 0}"
    name             = "${var.web["vm_name"]}"
    depends_on = ["nsxt_logical_switch.web", "null_resource.delaydestroy"]
    resource_pool_id = "${data.vsphere_resource_pool.pool.id}"
    datastore_id     = "${data.vsphere_datastore.datastore.id}"
    num_cpus = 1
    memory   = 4096
    guest_id = "${data.vsphere_virtual_machine.template.guest_id}"
    scsi_type = "${data.vsphere_virtual_machine.template.scsi_type}"
    # Attach the VM to the network data source that refers to the newly created logical switch
    network_interface {
      network_id = "${data.vsphere_network.terraform_web.id}"
    }
    disk {
	label = "${var.web["vm_name"]}.vmdk"
        size = 16
        thin_provisioned = true
    }
    clone {
	template_uuid = "${data.vsphere_virtual_machine.template.id}"

	# Guest customization to supply hostname and ip addresses to the guest
	customize {
	    linux_options {
		host_name = "${var.web["vm_name"]}"
		domain = "${var.web["domain"]}"
	    }
	    network_interface {
		ipv4_address = "${var.web["ip"]}"
		ipv4_netmask = "${var.web["mask"]}"
		dns_server_list = "${var.dns_server_list}"
		dns_domain = "${var.web["domain"]}"
	    }
	    ipv4_gateway = "${var.web["gw"]}"
	}
    }
    connection {
	type = "ssh",
	agent = "false"
	host = "${var.web["nat_ip"] != "" ? var.web["nat_ip"] : var.web["ip"]}"
	user = "${var.web["user"]}"
	password = "${var.web["pass"]}"
	script_path = "/root/tf.sh"
    }
    provisioner "remote-exec" {
	inline = [
	    "echo 'nameserver ${var.dns_server_list[0]}' >> /etc/resolv.conf",
            "echo 'search ${var.app["domain"]}' >>/etc/resolv.conf"
	]
    }
}

resource "nsxt_vm_tags" "vm2_tags" {
    count = "${var.quarantine == "0" && var.vm == "true" ? 1 : 0}"
    instance_id = "${vsphere_virtual_machine.webvm.id}"
    tag {
        scope = "${var.nsx_tag_scope}"
        tag = "${var.nsx_tag}"
    }
    tag {
        scope = "tier"
        tag = "web"
    }
    tag {
        scope = "os"
        tag = "ubuntu1604"
    }
    tag {
        scope = "velocloud/bandwidth"
        tag = "${var.bandwidth}"
    }
    tag {
        scope = "velocloud/routepolicy"
        tag = "edge2cloud"
    }
    tag {
        scope = "appdefense/scope"
        tag = "cpod-vcn"
    }
    tag {
        scope = "vcnapp/deploy-by"
        tag = "VCN-iApp"
    }
}

resource "null_resource" "ansible" {
  count = "${var.vm == "true" ? 1 : 0}"
  depends_on = ["vsphere_virtual_machine.webvm", "vsphere_virtual_machine.appvm"]
  provisioner "local-exec" {
    command = "ansible-playbook -i hosts testapp.yaml"
  }
}
