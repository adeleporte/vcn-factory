provider "vrni" {
  host      = "${var.vrni-provider["host"]}"
  username  = "${var.vrni-provider["username"]}"
  password  = "${var.vrni-provider["password"]}"
}


resource "vrni_application" "app1"{
  count = "${var.vrni == "true" ? 1 : 0}"
  name      =  "${var.vrni_appname}"

  tier {
    name                = "web"
    membershiptype      = "IP_RANGE"
    property            = "IP Address"
    value               = "${var.web["ip"]}"
  }

  tier {
    name                = "app"
    membershiptype      = "IP_RANGE"
    property            = "IP Address"
    value               = "${var.app["ip"]}"
  }

  tier {
    name                = "db"
    membershiptype      = "SEARCH"
    property            = "name"
    value               = "'MongoDb'"
  }

}

resource "vrni_threshold" "threshold1" {
  count = "${var.threshold == "true" ? 1 : 0}"
  name        = "MyVCNApp"
  application = "${var.vrni_appname}"
  application_id = "${vrni_application.app1.id}"
}




