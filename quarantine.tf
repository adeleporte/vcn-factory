resource "nsxt_vm_tags" "quarantine_tags" {
    count = "${var.quarantine == "1" && var.vm == "true" ? 1 : 0}"
    instance_id = "${vsphere_virtual_machine.webvm.id}"
    tag {
        scope = "${var.nsx_tag_scope}"
        tag = "${var.nsx_tag}"
    }
    tag {
        scope = "tier"
        tag = "web"
    }
    tag {
        scope = "os"
        tag = "ubuntu1604"
    }

    tag {
        scope = "quarantine"
        tag = "quarantine"
    }
    tag {
        scope = "velocloud/bandwidth"
        tag = "${var.bandwidth}"
    }
    tag {
        scope = "velocloud/routepolicy"
        tag = "edge2cloud"
    }
    tag {
        scope = "appdefense/scope"
        tag = "cpod-vcn"
    }
    tag {
        scope = "appdefense/alert-level"
        tag = "critical"
    }
}

resource "velocloud_fw_rule" "tf_quarantine_rules" {
  count = "${var.quarantine == "1" ? 1 : 0}"
  enterpriseid    = 123
  configurationid = "${data.velocloud_configuration.tf_config.id}"
  fwmodule        = "${data.velocloud_configuration.tf_config.firewall}"
  enabled         = false
  logging         = true

  rule {
    name            = "My VCN App"
    dip             = "${var.web["ip"]}"
    dmask           = "255.255.255.255"
    dport           = -1
    proto           = 6
    action          = "deny"
  }

}

